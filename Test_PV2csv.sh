#!/usr/bin/env bash

RED=31
GREEN=32

cprint () {
    local color="$1"
    shift
    printf "\e[%dm%s\e[0m" "$color" "$*"
}

print_result () {
    result=$?
    printf "["
    if [ $result -eq 0 ]
    then
        cprint $GREEN SUCCESS
    else
        cprint $RED FAILURE
    fi
    printf "] "
}

for dir in "$@"
do
    PV2csv.py --englishe "$dir"/*.pdf >/tmp/test_data.csv
    diff "$dir"/*.csv /tmp/test_data.csv >/dev/null
    print_result
    echo " $dir"
done
