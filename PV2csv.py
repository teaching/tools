#!/usr/bin/env python3

import numbers
import re
import subprocess
import sys
import getopt

formation="GB"
description_start = "Identification des étudiants"
PV_end = "Nombre total d'étudiants :"
verboseDebug = False
parseResult = False
resultName = None


def help():
	print(f"""Usage :
	{sys.argv[0]} [ --help ] [ --debug ] [ --englishe ] [ --prefix PREF ] [ --two-lines ]
				[ --name name ] file_1.pdf [ [ --name name ] file_2.pdf ... ]
	
	Parse each “procès verbal” which name is given as an argument, assuming the présentation produced by “Apogée”
	and outputs all the grades as a large CSV. Accepts the following options (or any non ambiguous prefix):
	--help			prints this help and exits
	--debug			prints debug information
	--englishe		outputs the CSV using dot for decimal separator and comma for fields separator
	--name name		name for the 'Résultat' in the following file
	--prefix PREF	uses PREF as a prefix for the formation instead of {formation}, and try to guess the semestre from the name found
	--result		parses result (ADM, AJ, ...) in addition to grades
	CAUTION: this script parses the output of pdftotext. This makes certain things such as UE descriptions especially difficult to
	parse, to they might be cut inappropriately.
	""")
	exit(1)

def debug(*kargs, **kwargs):
	if verboseDebug:
		print("[DEBUG]: ", *kargs, **kwargs, file=sys.stderr)

def error(*kargs, **kwargs):
	print("[ERROR]: ", *kargs, **kwargs, file=sys.stderr)
	exit(1)

class State:
	Topic = 1
	UECodes = 2
	Description1 = 4
	Description2 = 8
	StudentsPad = 16
	StudentsNum = 32
	StudentsGrades = 64
	StudentsName = 128
	StudentsDone = 256

class UE:
	def __init__(self, name, start, end):
		self.name = name
		self.start = start
		self.end = end
		self.description = ""
		self.hasTwoSessions = False

	def contains(self, point):
		return point >= self.start and point < self.end

	def compare(self, start, end):
		if self.end <= start:
			return -1
		if self.start >= end:
			return 1
		return 0

	def __eq__(self, other):
		return self.name == other.name

class Score:
	def __init__(self):
		self.value = [None, None]

	def __eq__(self, other):
		return self.value[0] == other.value[0] and self.value[1] == other.value[1]

	def set(self, value, jury=0):
		if self.value[jury] is not None and self.value[jury] != value:
			raise Exception("Incoherent grades")
		self.value[jury] = value

	def get(self):
		if self.value[0] is None:
			if self.value[1] is None:
				return None
			else:
				raise Exception("Incomplete grade, jury points only")
		if self.value[1] is None:
			return self.value[0]
		return "{:g}".format(float(self.value[0]) + float(self.value[1]))


class Grade:
	def __init__(self, ue):
		self.ue = ue
		self.score = [None, None]

	def has2sessions(self):
		return self.score[0] is not None and self.score[1] is not None and self.score[0] != self.score[1]

	def set(self, grade, session, jury=0):
		if self.score[session-1] is None:
			self.score[session-1] = Score()
		self.score[session-1].set(grade, jury)

	def get(self, session=None):
		if session:
			if self.score[session-1] is not None:
				return self.score[session-1].get()
		else:
			if self.score[0] is not None:
				return self.score[0].get()
			elif self.score[1] is not None:
				return self.score[1].get()
		return ""

class Student:
	def __init__(self, number):
		self.number = number
		self.grades = {}
		self.hasSession = { 1: False, 2: False }

	def hasTwoSessions(self):
		return self.hasSession[1] and self.hasSession[2]

def searchForUEIndex(ueList, start, end):
	index = 0
	while index < len(ueList)-1 and ueList[index].compare(start, end) < 0 and ueList[index+1].compare(start, end) < 1:
		index += 1
	return ueList[index]

ueList = []
students = {}
englishe = False
twoLines = False
grades_line = []

options, argsLeft = getopt.getopt(sys.argv[1:], "hden:p:rt", ["help", "debug", "englishe", "name=", "prefix=", "result", "two-lines"])
for pair in options:
	if pair[0] == "-h" or pair[0] == "--help":
		help()
	elif pair[0] == "-d" or pair[0] == "--debug":
		verboseDebug = True
	elif pair[0] == "-e" or pair[0] == "--englishe":
		englishe = True
	elif pair[0] == "-n" or pair[0] == "--name":
		resultName = pair[1]
	elif pair[0] == "-p" or pair[0] == "--prefix":
		formation = pair[1]
	elif pair[0] == "-r" or pair[0] == "--result":
		parseResult = True
	elif pair[0] == "-t" or pair[0] == "--two-lines":
		twoLines = True

while len(argsLeft):
	filename = argsLeft.pop(0)
	if filename == "-n" or filename == "--name":
		resultName = argsLeft.pop(0)
		filename = argsLeft.pop(0)
	currentStudents = {}
	currentUeList = []
	state = State.Topic
	proc = subprocess.Popen(["pdftotext", "-layout", filename, "-"], stdout=subprocess.PIPE)
	for line in proc.stdout:
		line = line.decode("utf-8")
		debug("Line read (", state, "): " + line, end="")
		# We skip blank line that might be inserted by pdftotext
		match = re.match(r"^\s*$", line)
		if match:
			debug("This is a blank line, continuing to the next line")
		elif state == State.Topic:
			# We assume that the PV title is of the form
			# Session <number> bla bla {formation}XXX<session>XXX
			for match in re.finditer(f"Session ([0-9])", line):
				debug("Session : " + match.group(1))
				session = int(match.group(1))
				pattern = f"{formation}"+r"\D*(\d+)\D"
				debug(f"Formation pattern {pattern}")
				for match2 in re.finditer(pattern, line):
					debug("Element : " + match2.group(0) + " semester " + match2.group(1))
					semester = match2.group(1)
					state = State.UECodes
		elif state >= State.UECodes and state <= State.Description2:
			# We know that the UE codes are just before the description and we must detect it this way
			# because there might be garbage inbetween the topic and the UE codes
			if state == State.UECodes:
				found = line.find(description_start)
				pos = found + len(description_start)
				if found > -1:
					# UE codes are separated by at least two spaces (this enables UE codes containing spaces)
					for match in re.finditer(r"(?:\S+\s)*\S+", previous_line):
						ue = UE(match.group(0), match.start(0), match.end(0))
						currentUeList.append(ue)
						if ue not in ueList:
							ueList.append(ue)
						debug("Matched : " + ue.name + " from ", ue.start, " to ", ue.end)
					state = State.Description1
				else:
					if line.find(PV_end) > -1:
						population = int(line.split(':')[1])
						assert(population == len(currentStudents))
						debug(f"{population} students found (as expected)")
					previous_line = line
			else:
				pos = 0
			if state > State.UECodes:
				# UEs descriptions are on two lines so, with the shift at the end, we pass twice here
				dans_mot = False
				while pos < len(line):
					if line[pos].isspace():
						if dans_mot:
							mot = line[debut_mot:pos].translate(str.maketrans({',':' ', ';':' '}))
							debug("mot trouvé " + mot)
							if mot == "Résultat":
								if resultName is None:
									resultName = f"S{semester}"
								ue = UE(f"{mot} {resultName}", debut_mot, pos)
								currentUeList.insert(0, ue)
								if ue not in ueList:
									ueList.insert(0, ue)
							else:
								currentUe = searchForUEIndex(currentUeList, debut_mot, pos)
								currentUe.description += " " + mot
							dans_mot = False
					else:
						if not dans_mot:
							debut_mot = pos
							dans_mot = True
					pos+=1
				state <<= 1
		elif state >= State.StudentsPad and state <= State.StudentsDone:
			# Here this might get complicated, but :
			# - in StudentsPad, we are waiting for the first student id or the first grades marker
			# - in StudentsNum, we have seen the grades marker and are waiting for the student id
			# - in StudentsGrades, we have seen the student id and are waiting for the grades marker
			# - in StudentsName, we have seen both student id and grades marker, we are waiting for the student name
			# - in StudentsDone, we have seen all the information related to a student, we are ready for the next one
			# We expect stundent id and grades marker on the same line, in this order, but this is not always the case :
			# the marker can appear one line before and some grades might come after the line containing the marker
			pos = 0
			match = re.match(r".*N°\s*étudiant\s*:\s*([0-9]+)", line)
			if match:
				debug("Matched student num " + match.group(1) + " from ", match.start(1), " to ", match.end(1), " (ends at ", match.end(0), ")")
				if state >= State.StudentsGrades and state < State.StudentsDone:
					error(f"Student id found in state {state}")
				num = match.group(1)
				pos = match.end(1)
				if num not in currentStudents:
					currentStudents[num] = True
				if num in students:
					currentStudent = students[num]
				else:
					currentStudent = Student(num)
					students[num] = currentStudent
				if state == State.StudentsPad or state == State.StudentsDone:
					state = State.StudentsGrades
				else:
					state = State.StudentsName
			# grades beginning marker can appear at anytime during students search, even before the student id
			match = re.match(r".*(Note/Pt. jury)", line)
			if match:
				debug("Matched grades beginning marker " + match.group(1) + " from ", match.start(1), " to ", match.end(1))
				if state == State.StudentsNum or state == State.StudentsName:
					error(f"Grades marker found in state {state}")
				pos = match.end(1)
				if state == State.StudentsPad or state == State.StudentsDone:
					state = State.StudentsNum
				else:
					state = State.StudentsName
			# If we are in StudentsDone at this point, it means we have completed the parsing for one student and we did
			# not find one of the markers for the next one
			if state == State.StudentsDone:
				if len(grades_line):
					error("grades_line not empty in StudentsDone state")
				currentUeList = []
				grades_line = []
				state = State.Topic
			# Otherwise, we can pursue with the parsing of the detected student
			else:
				match = re.match(r"(.*)Rés\s*/\s*Crédits", line)
				if match:
					name = match.group(1).strip()
					debug("Matched name " + name)
					if state < State.StudentsName:
						error(f"Name found in state {state}")
					currentStudent.name = name
					# we use the opportunity to parse the result (ADM/AJ/...)
					if parseResult:
						pos = match.end(0)
						for decision in re.finditer(r'[A-Z][A-Z]+', line[pos:]):
							result = decision.group(0)
							debug("Result " + result + " found")
							currentUe = searchForUEIndex(currentUeList, decision.start(0) + pos, decision.end(0) + pos)
							nameUE = currentUe.name
							if nameUE not in currentStudent.grades:
								currentStudent.grades[nameUE] = Grade(nameUE)
								formerResult = ""
							else:
								formerResult = currentStudent.grades[nameUE].get(session) + " / "
							currentStudent.grades[nameUE].set(f"{formerResult}{result}", session)
					state = State.StudentsDone
				if state != State.StudentsPad and state != State.StudentsDone:
					# We add the rest of the line as a grades line
					debug(f"Appending grades line after {pos}")
					grades_line.append((line, pos))
				if state > State.StudentsNum and state != State.StudentsDone and len(grades_line) > 0:
					# we search for grades in all the relevant lines
					for (line, pos) in grades_line:
						score = r'(?:[0-9]+(?:\.[0-9]+)?)'
						status = r'(?:DEF)|(?:AB[JI])'
						# for grade in re.finditer(r'(' + score + r'|' + status + r')(?:\s*/\s*(' + score + '))?', line[pos:]):
						for grade in re.finditer(r'(/)?\s+(' + score + r'|' + status + r')', line[pos:]):
							start = grade.start(2)+pos
							end = grade.end(2)+pos
							currentUe = searchForUEIndex(currentUeList, start, end)
							nameUE = currentUe.name
							result = grade.group(2)
							jury = 1 if grade.group(1) else 0;
							debug("Grade " + result + " found " + ("(jury points) " if jury else "") +
							  	f"from {start} to {end} for UE {nameUE}")
							if nameUE not in currentStudent.grades:
								currentStudent.grades[nameUE] = Grade(nameUE)
							currentStudent.grades[nameUE].set(result, session, jury)
							currentStudent.hasSession[session] = True
					grades_line = []
		else:
			error("Internal bug, invalid state")
	resultName = None

separator = "," if englishe else ";"
def translate_entry(entry):
	global englishe
	if not englishe:
		entry = entry.translate({ord('.'): ','})
	return entry

print("Numéro" + separator + "Etudiant", end="")
if twoLines:
	print(separator + "Session", end="")
for ue in ueList:
	for num in students:
		if ue.name in students[num].grades:
			if students[num].grades[ue.name].has2sessions():
				ue.hasTwoSessions = True
				break
	if ue.hasTwoSessions and not twoLines:
		print(separator + ue.name + " session 1", end="")
		print(separator + ue.name + " session 2", end="")
	else:
		print(separator + ue.name, end="")
print("")

print(separator, end="")
if twoLines:
	print(separator, end="")
for ue in ueList:
	print(separator + ue.description, end="")
	if ue.hasTwoSessions and not twoLines:
		print(separator + ue.description, end="")
print("")

for num in students:
	student = students[num]
	if (twoLines):
		for session in (1,2):
			if student.hasSession[session]:
				print(f"{student.number}{separator}{student.name}{separator}{session}", end="")
				for ue in ueList:
					if ue.name in student.grades:
						grade = student.grades[ue.name].get(session)
						print(separator + translate_entry(grade), end="")
					else:
						print(separator, end="")
				print("")
	else:
		print(f"{student.number}{separator}{student.name}", end="")
		for ue in ueList:
			if ue.name in student.grades:
				if ue.hasTwoSessions:
					for session in [1, 2]:
						grade = student.grades[ue.name].get(session)
						print(separator + translate_entry(grade), end="")
				else:
					grade = student.grades[ue.name].get()
					print(separator + translate_entry(grade), end="")
			else:
				print(separator, end="")
				if ue.hasTwoSessions:
					print(separator, end="")
		print("")
