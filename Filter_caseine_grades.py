#!/usr/bin/env python3
import functools
import sys

import pandas as pd
import numpy as np

if len(sys.argv) < 2:
	print(f"""Usage :
{sys.argv[0]} csv_file

Clean a grades file got from caséine by keeping only 'First name', 'Last name' and grades columns,
renaming columns into simpler titles, excluding students that have no grade at all and replacing
missing grades with 0. Writes the result into 'Notes_Caseine.csv'""")
	sys.exit(1)
df = pd.read_csv(sys.argv[1])
student = ["First name", "Last name"]
names = [x for x in df.columns if "Virtual programming lab" in x]
renaming = { x: x.split(": ",2)[1].removesuffix(" (Real)") for x in names}
df = df.rename(columns=renaming)
names = list(renaming.values())
selected = df.loc[:,student]
for name in names:
	selected[name] = pd.to_numeric(df[name], errors='coerce')
# Test is true when at least one of the evaluations is not NaN
selected["Test"] = selected[names].apply(lambda row: functools.reduce(lambda a, b: a and b, np.isnan(row)), axis=1)
graded = selected.loc[selected["Test"] == False, student+names].fillna(0)
print(graded.to_string())
graded.to_csv("Notes_Caseine.csv", index=False)
